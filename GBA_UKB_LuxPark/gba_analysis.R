
## UK biobank 
# function to perform PRS <> carrier analysis
calc_per_gene <- function(gene_name){
   park_variants_prs %>% 
      mutate(sintu_interpretation=if_else(sintu_interpretation=="","NA",sintu_interpretation)) %>%
      mutate(carrier_sintu=if_else(sintu_interpretation=="NA",0,1)) %>% 
      filter( Gene.refGene %in% c("GBA","")) %>%   
      filter(sintu_interpretation !="VUS" | carrier_sintu==0) %>% 
      filter(PredictedSuperpopulation == "EUR") %>% 
      mutate(percentile = ntile(prs_37,3),
             percentile = factor(percentile),
             
             percentile= fct_recode(percentile,
                                    "Low"   = "1",
                                    "Intermediate"   = "2" ,
                                    "High"   = "3") ,
             carrier_sintu = factor(carrier_sintu),
             carrier_sintu= fct_recode(carrier_sintu, 
                                     "noncarrier" = "0",
                                     "carrier"     = "1"),
              ill = factor(ill),
              ill= fct_recode(ill, 
                              "nonhistory" = "0",
                              "history"     = "1"),
             hist_ntile = factor(paste(ill,percentile,sep = "_")),
             car_ntile = factor(paste(carrier_sintu,percentile,sep = "_")),
             hist_car_nile= factor(paste(ill,carrier_sintu,percentile,sep = "_")),
      ) %>%
      group_by(hist_ntile) %>% 
      mutate(average_prs = mean(prs_37)) %>% 
      ungroup  -> ukb_pd
   
   
   
   glm(pheno ~ carrier_sintu + prs_37 + age + ill +
          genetic_principal_components_f22009_0_1+
          genetic_principal_components_f22009_0_2+
          genetic_principal_components_f22009_0_3+
          genetic_principal_components_f22009_0_4 
       , data = ukb_pd, family=binomial()) -> model_pd_hist_car
   
   ukb_pd %>% 
      mutate(ill=as.numeric(ill)) %>%
      group_by(car_ntile) %>% 
      mutate(prs_37= mean(prs_37)) %>% 
      ungroup %>% 
      mutate(ill=mean(ill),
         age = mean(age),
             genetic_principal_components_f22009_0_1= mean(genetic_principal_components_f22009_0_1),
             genetic_principal_components_f22009_0_2= mean(genetic_principal_components_f22009_0_2),
             genetic_principal_components_f22009_0_3= mean(genetic_principal_components_f22009_0_3),
             genetic_principal_components_f22009_0_4= mean(genetic_principal_components_f22009_0_4)) -> new_pd_hist_car
   
   
   preds_pd_hist_car <- predict(model_pd_hist_car,new_pd_hist_car,  type = "link", se.fit = TRUE)
   
   critval <- 1.96 ## approx 95% CI
   upr <- preds_pd_hist_car$fit + (critval * preds_pd_hist_car$se.fit)
   lwr <- preds_pd_hist_car$fit - (critval * preds_pd_hist_car$se.fit)
   fit <- preds_pd_hist_car$fit
   
   
   fit2 <- model_pd_hist_car$family$linkinv(fit)
   upr2 <- model_pd_hist_car$family$linkinv(upr)
   lwr2 <- model_pd_hist_car$family$linkinv(lwr)
   # fam <- family(model_prs4)
   # ilink <- fam$linkinv
   # 
   new_pd_hist_car <- mutate(new_pd_hist_car,
                             fit_link  = fit2,
                             upr = upr2,
                             lwr = lwr2 )
   
   
   new_pd_hist_car %>% 
      group_by(car_ntile) %>% 
      summarise(pred_or = fit_link/new_pd_hist_car %>%  filter(car_ntile == "noncarrier_Intermediate") %>%
                   distinct(fit_link) %>% pull,
                upr = upr/new_pd_hist_car%>%  filter(car_ntile == "noncarrier_Intermediate") %>%
                   distinct(upr) %>% pull,
                lwr = lwr/new_pd_hist_car%>%  filter(car_ntile == "noncarrier_Intermediate") %>%
                   distinct(lwr) %>% pull,
                car_ntile=car_ntile
      ) %>% 
      distinct() -> pd_hist_car_final
   
}


calc_per_gene_interp <- function(gene_name,interpretation){

   park_variants_prs %>% 
      mutate(sintu_interpretation=if_else(sintu_interpretation=="","NA",sintu_interpretation)) %>%
      mutate(carrier_sintu=if_else(sintu_interpretation=="NA",0,1)) %>% 
      filter( Gene.refGene %in% c(gene_name,"")) %>%   
      filter(sintu_interpretation !="VUS" | carrier_sintu==0) %>% 
      filter( sintu_interpretation %in% c(interpretation,"NA")) %>%   
      filter( Gene.refGene %in% c("GBA","")) %>%  
      filter(PredictedSuperpopulation == "EUR") %>% 
      mutate(percentile = ntile(prs_37,3),
             percentile = factor(percentile),
             percentile= fct_recode(percentile,
                               "Low"   = "1",
                               "Intermediate"   = "2" ,
                               "High"   = "3") ,
             carrier_sintu = factor(carrier_sintu),
             carrier_sintu= fct_recode(carrier_sintu, 
                                  "noncarrier" = "0",
                                  "carrier"     = "1"),
              ill = factor(ill),
              ill= fct_recode(ill, 
                              "nonhistory" = "0",
                              "history"     = "1"),
             hist_ntile = factor(paste(ill,percentile,sep = "_")),
             car_ntile = factor(paste(carrier_sintu,percentile,sep = "_")),
             hist_car_nile= factor(paste(ill,carrier_sintu,percentile,sep = "_")),
      ) %>%
      group_by(hist_ntile) %>% 
      mutate(average_prs = mean(prs_37)) %>% 
      ungroup  -> ukb_pd
   
   
   
   glm(pheno ~ carrier_sintu + prs_37 + age + ill +
          genetic_principal_components_f22009_0_1+
          genetic_principal_components_f22009_0_2+
          genetic_principal_components_f22009_0_3+
          genetic_principal_components_f22009_0_4 
       , data = ukb_pd, family=binomial()) -> model_pd_hist_car
   
   ukb_pd %>% 
      mutate(ill=as.numeric(ill)) %>%
      group_by(car_ntile) %>% 
      mutate(prs_37= mean(prs_37)) %>% 
      ungroup %>% 
      mutate(ill=mean(ill),
         age = mean(age),
             genetic_principal_components_f22009_0_1= mean(genetic_principal_components_f22009_0_1),
             genetic_principal_components_f22009_0_2= mean(genetic_principal_components_f22009_0_2),
             genetic_principal_components_f22009_0_3= mean(genetic_principal_components_f22009_0_3),
             genetic_principal_components_f22009_0_4= mean(genetic_principal_components_f22009_0_4)) -> new_pd_hist_car
   
   
   preds_pd_hist_car <- predict(model_pd_hist_car,new_pd_hist_car,  type = "link", se.fit = TRUE)
   
   critval <- 1.96 ## approx 95% CI
   upr <- preds_pd_hist_car$fit + (critval * preds_pd_hist_car$se.fit)
   lwr <- preds_pd_hist_car$fit - (critval * preds_pd_hist_car$se.fit)
   fit <- preds_pd_hist_car$fit
   
   
   fit2 <- model_pd_hist_car$family$linkinv(fit)
   upr2 <- model_pd_hist_car$family$linkinv(upr)
   lwr2 <- model_pd_hist_car$family$linkinv(lwr)
   # fam <- family(model_prs4)
   # ilink <- fam$linkinv
   # 
   new_pd_hist_car <- mutate(new_pd_hist_car,
                             fit_link  = fit2,
                             upr = upr2,
                             lwr = lwr2 )
   
   
   new_pd_hist_car %>% 
      group_by(car_ntile) %>% 
      summarise(pred_or = fit_link/new_pd_hist_car %>%  filter(car_ntile == "noncarrier_Intermediate") %>%
                   distinct(fit_link) %>% pull,
                upr = upr/new_pd_hist_car%>%  filter(car_ntile == "noncarrier_Intermediate") %>%
                   distinct(upr) %>% pull,
                lwr = lwr/new_pd_hist_car%>%  filter(car_ntile == "noncarrier_Intermediate") %>%
                   distinct(lwr) %>% pull,
                car_ntile=car_ntile
      ) %>% 
      distinct() -> pd_hist_car_final
   
}



## run functions

calc_per_gene("GBA") %>% mutate(gene = "GBA") -> gba


calc_per_gene_interp("GBA","Severe") %>% mutate(gene = "GBA",severity="Severe")   %>% 
  separate(car_ntile,c("carrier","prs"),"_") %>%
   mutate(gene=if_else(carrier=="noncarrier","noncarrier",gene)) %>%
   filter(gene=="GBA") %>% 
   mutate(study="UK Biobank") -> gba_severe

calc_per_gene_interp("GBA","Mild") %>% mutate(gene = "GBA",severity="Mild")   %>% 
  separate(car_ntile,c("carrier","prs"),"_") %>%
   mutate(gene=if_else(carrier=="noncarrier","noncarrier",gene))  %>%
   filter(gene=="GBA")%>%
   mutate(study="UK Biobank") -> gba_mild

calc_per_gene_interp("GBA","Risk") %>% mutate(gene = "GBA",severity="Low")  %>%  
  separate(car_ntile,c("carrier","prs"),"_") %>%
   mutate(gene=if_else(carrier=="noncarrier","noncarrier",gene))%>% 
   mutate(study="UK Biobank")  -> gba_low


# plotting 
gba_severe %>% 
   rbind(gba_mild) %>% 
   rbind(gba_low) %>% 
   mutate(severity=if_else(carrier=="noncarrier","noncarrier",severity)) %>% 
   distinct(prs,severity,.keep_all = T) %>%
   mutate(severity=fct_reorder(severity,desc(pred_or))) %>% 
   ggplot(aes(prs,pred_or,color= carrier,linetype=severity)) +
   geom_point(size = 1)+
   geom_line(size = 0.8)+
   geom_errorbar(aes(ymin = lwr, ymax = upr), width = 0.2,alpha=0.3) +
   # geom_ribbon(aes(ymin=lwr, ymax=upr),
   #             alpha = 0.2,linetype = 0) +
   #scale_x_continuous(breaks = seq(0, 10, by = 2))+
   scale_y_continuous(trans='log10',
                      breaks = c(0.5,1,2,4,8)) +
   guides(color=guide_legend(override.aes=list(alpha=10,size=2,fill=NA))) +
   labs(y= "Odds ratio for PD", x = "PRS percentiles") +
   theme_pubr(legend = c(0.22, 0.83) )+
   theme(axis.line = element_line(size=1),
         axis.ticks.length=unit(.25, "cm"),
         axis.ticks = element_line(size=1),
         legend.text = element_text(size=16),
         legend.title = element_text(size=16),
         axis.text = element_text(size=16),
         axis.title = element_text(size=16,margin = margin(t = 50, r = 50, b = 50, l =50)),
         strip.text.x = element_text(
            size = 12, color = "black", face = "bold"
         ),
         strip.background = element_rect(colour="black",fill= "white",size = 1))


### luxpark

library(ggrepel)
library(dplyr)
library(tidyr)
library(forcats)
library(ggpubr)
library(ggplot2)

#input demographic data


pd_22_june <- readxl::read_xlsx("PRS_final.17_07_2023.LuxPARK.xlsx")
pd_22_june_age <- readxl::read_xlsx("age.xlsx") %>% 
  dplyr::select(Subject_ID,age_in_2023)

pd_22_june <- fread("GBA_PRS_21062023.txt") %>%
  left_join(pd_22_june_age,by=c("iid"="Subject_ID"))


LP_data <- readxl::read_xlsx("LuxPARK_update_15122022_formatted.xlsx")
LP_data[,c(2,3,13,14)] <- lapply(LP_data[,c(2,3,13,14)], as.factor)
LP_data <- filter(LP_data, LP_data$diagnostic == "PD" | LP_data$diagnostic == "PDD" | LP_data$diagnostic == "HC")
#input PCA
LP_pca <- read.table("plink.eigenvec")
LP_pca <- LP_pca[,-2]
colnames(LP_pca) <- c("iid","pc1","pc2","pc3","pc4","pc5")
#merge PCA and demo/clin data
LP_data <- merge(LP_data, LP_pca, by = "iid")
rm(LP_pca)
#input PRS data
PRS_best <- read.table("LP_PRS_42snps.all_score", header = T) %>% dplyr::select(c(2,3))
colnames(PRS_best) <- c("iid", "PRS_42")
LP_data <- merge(LP_data, PRS_best, by = "iid")
rm(PRS_best)

LP_data %>% 
   filter(is.na(`LRRK2 (Dominant)`)) %>% 
   filter(is.na(`PRKN (Recessive)`)) -> 

##########ntile Whole genome PRS
   
LP_data <- pd_22_june  %>% 
   mutate(PRS_42=as.numeric(scale(as.numeric(PRS_42))),
          age=as.numeric(age)) %>%
   mutate(age_filter=case_when(
      age<60 & aff==0 ~ 2,
      age>=60 & aff==0 ~ 0,
      TRUE~1))
LP_data$carrier <- ifelse(LP_data$GBAp_carrier == 0,"noncarrier","carrier")
LP_data$carrier <- as.factor(LP_data$carrier)
LP_data %>% 
   mutate(FH=as.double(FH)) %>%
   mutate(FH=if_else(is.na(FH),0,FH)) -> LP_data

LP_data %>%
   mutate(
      percentile = ntile(PRS_42,3),
                   percentile = factor(percentile),

                   percentile= fct_recode(percentile,
                                     "Low"   = "1",
                                     "Intermediate"   = "2" ,
                                    # "Intermediate"   = "3" ,
                                    # "Intermediate"   = "4" ,
                                     "High"   = "3") ,
                   car_ntile = factor(paste(carrier,percentile,sep = '_')),
                   hist_car_nile= factor(paste(FH,carrier,percentile,sep = "_"))) -> new_prova

## carrier
model_prs <- glm(aff ~ carrier + PRS_42  + age + FH , data = new_prova, family=binomial())
new_data <- new_prova %>%
   
   group_by(car_ntile) %>%
   mutate(PRS_42 = mean(PRS_42)) %>%
   ungroup %>%
   mutate( FH=mean(FH,na.rm=TRUE), 
      age = mean(age)
       ,pc1= mean(pc1),pc2= mean(pc2),pc3= mean(pc3),
            pc4= mean(pc4),pc5= mean(pc5)
      )




preds <- predict(model_prs,new_data,  type = 'link', se.fit = TRUE)
critval <- 1.96 ## approx 95% CI
upr <- preds$fit + (critval * preds$se.fit)
lwr <- preds$fit - (critval * preds$se.fit)
fit <- preds$fit
fit2 <- model_prs$family$linkinv(fit)
upr2 <- model_prs$family$linkinv(upr)
lwr2 <- model_prs$family$linkinv(lwr)

new_data <- mutate(new_data,fit_link  = fit2,upr = upr2,lwr = lwr2 )
new_data %>%
   group_by(car_ntile) %>%
   summarise(pred_or = fit_link/new_data %>%  filter(car_ntile== 'noncarrier_Intermediate') %>%
                distinct(fit_link) %>% pull,
             upr = upr/new_data %>%  filter(car_ntile == 'noncarrier_Intermediate') %>%
                distinct(upr ) %>% pull,
             lwr = lwr/new_data %>%  filter(car_ntile == 'noncarrier_Intermediate') %>%
                distinct(lwr ) %>% pull,
             car_ntile=car_ntile) %>%
   distinct() -> prova_final
prova_plot = prova_final %>%
   separate(car_ntile,c('carrier','prs'),'_') %>%
   mutate(
      #prs = as.numeric(prs),
          prs_carrier = paste(prs,carrier,sep=' and '),
          prs_carrier = fct_reorder(prs_carrier , pred_or,.desc = T))

wg_plot <- ggplot(prova_plot, aes(prs,pred_or,color=carrier)) +
   geom_point(size = 2)+
   geom_line(size = 0.8)+
   geom_hline(yintercept = 1, linetype='dotted', color = 'grey50', size=1) +
   guides(colour = guide_legend(order = 1),
          shape = guide_legend(order = 2)) +
   theme_minimal()+
   theme(legend.text=element_text(size=15),
         legend.title = element_text(size=15),
         axis.line = element_line(size=0.9),
         axis.title   = element_text(size=15),
         axis.text = element_text(size=15),
         axis.ticks = element_line(size=1),
         axis.text.y = element_text(hjust=0,face = 'italic'),
         axis.ticks.length=unit(0.25, 'cm'),
         axis.title.x = element_text(margin = margin(t = 10, r = 0, b =10, l = 0)),
         legend.key.size = unit(1.5, 'lines'))+
   labs(y = "Odds ratio for PD" , x = "Polygenic score")
wg_plot





prova_plot %>% 
   dplyr::select(-prs_carrier) %>%
   mutate(carrier=if_else(carrier=="carrier","carrier","noncarrier")) %>% 
   mutate(study="LuxPark") -> luxpark_fin

gba %>%
   separate(car_ntile,c("carrier","prs"),"_") %>%
  # mutate(prs = as.numeric(prs)) %>%
   dplyr::select(-gene) %>% 
   mutate(study="Uk Biobank")-> ukb_fin

png("/home/ubuntu/Mount/projects/ukbiobank_prs_rare/manuscript_pd/GBA/figure1_fin2.png", units="in", width= 9, height=6, res=300)


ukb_fin%>% 
   rbind(luxpark_fin) %>%
   mutate(study=factor(study,levels = c("Uk Biobank","LuxPark"))) %>% 
   mutate(prs=factor(prs,levels = c("Low","Intermediate","High"))) %>% 
   ggplot(aes(pred_or,prs,color=carrier)) +
   geom_point(size = 2, position=position_dodge(width = 0.2))+
   geom_errorbarh(aes(xmin = lwr, xmax = upr), height = 0,alpha=0.5, position=position_dodge(width = 0.2)) +
   # geom_ribbon(aes(ymin=lwr, ymax=upr),
   #             alpha = 0.2,linetype = 0) +
   #geom_line(size = 0.8)+
  # coord_flip() +
   geom_vline(xintercept = 1, linetype='dotted', color = 'grey50', size=1) +
   #, scales = "free_x"
   facet_wrap(~study) +
   
   guides(colour = guide_legend(order = 1),
          shape = guide_legend(order = 2)) +
   theme_bw()+
   theme(legend.text=element_text(size=15),
         legend.title = element_text(size=15),
         axis.line = element_line(size=0.9),
         axis.title   = element_text(size=15),
         axis.text = element_text(size=15,colour="black"),
         axis.ticks = element_line(size=1),
         axis.text.y = element_text(hjust=0,face = 'italic'),
         axis.ticks.length=unit(0.25, 'cm'),
         axis.title.x = element_text(margin = margin(t = 10, r = 0, b =10, l = 0)),
         legend.key.size = unit(1.5, 'lines'))+
   labs(x = "Odds ratio for PD" , y = "PRS categories") -> prs_car

set_palette(prs_car,c("#ED0000FF","#00468BFF","#FDAF91FF"))


dev.off()

## carrier


model_prs <- glm(aff ~ FH+ carrier + PRS_42  + 
                    pc1 + pc2 + pc3 + pc4 + pc5, data = new_prova, family=binomial())
new_data <- new_prova_n %>%
   group_by(hist_car_nile) %>%
   mutate(PRS_42 = mean(PRS_42)) %>%
   ungroup %>%
   mutate( pc1= mean(pc1),pc2= mean(pc2),pc3= mean(pc3),
           pc4= mean(pc4),pc5= mean(pc5))
preds <- predict(model_prs,new_data,  type = 'link', se.fit = TRUE)
critval <- 1.96 ## approx 95% CI
upr <- preds$fit + (critval * preds$se.fit)
lwr <- preds$fit - (critval * preds$se.fit)
fit <- preds$fit
fit2 <- model_prs$family$linkinv(fit)
upr2 <- model_prs$family$linkinv(upr)
lwr2 <- model_prs$family$linkinv(lwr)

new_data <- mutate(new_data,fit_link  = fit2,upr = upr2,lwr = lwr2 )
new_data %>%
   group_by(hist_car_nile) %>%
   summarise(pred_or = fit_link/new_data %>%  filter(hist_car_nile== '0_noncarier_3') %>%
                distinct(fit_link) %>% pull,
             upr = upr/new_data %>%  filter(hist_car_nile == '0_noncarier_3') %>%
                distinct(upr ) %>% pull,
             lwr = lwr/new_data %>%  filter(hist_car_nile == '0_noncarier_3') %>%
                distinct(lwr ) %>% pull,
             hist_car_nile=hist_car_nile) %>%
   distinct() -> prova_final_hist

## Severity

calc_per_gene_inter_lux <- function(interpretation){
   LP_data1  %>% 
      mutate(Severity_GBA=if_else(Severity_GBA=="mild","low-risk",Severity_GBA)) %>%   
       filter( Severity_GBA %in% c(interpretation,NA)) %>%  
      mutate(percentile = ntile(PRS_42,3),
             percentile = factor(percentile),
             percentile = factor(percentile),
             percentile= fct_recode(percentile,
                                    "Low"   = "1",
                                    "Intermediate"   = "2" ,
                                    "High"   = "3") ,

             car_ntile = factor(paste(carrier,percentile,sep = '_'))) -> new_prova
   
   
   
   model_prs <- glm(aff ~ carrier + PRS_42  + age 
                    +
                       pc1 + pc2 + pc3 + pc4
                    , data = new_prova, family=binomial())
   new_data <- new_prova %>%
      group_by(car_ntile) %>%
      mutate(PRS_42 = mean(PRS_42)) %>%
      ungroup %>%
      mutate( age = mean(age)
              ,pc1= mean(pc1),pc2= mean(pc2),pc3= mean(pc3),
              pc4= mean(pc4)
              )
   preds <- predict(model_prs,new_data,  type = 'link', se.fit = TRUE)
   critval <- 1.96 ## approx 95% CI
   upr <- preds$fit + (critval * preds$se.fit)
   lwr <- preds$fit - (critval * preds$se.fit)
   fit <- preds$fit
   fit2 <- model_prs$family$linkinv(fit)
   upr2 <- model_prs$family$linkinv(upr)
   lwr2 <- model_prs$family$linkinv(lwr)
   
   new_data <- mutate(new_data,fit_link  = fit2,upr = upr2,lwr = lwr2 )
   new_data %>%
      group_by(car_ntile) %>%
      summarise(pred_or = fit_link/new_data %>%  filter(car_ntile== 'noncarrier_Intermediate') %>%
                   distinct(fit_link) %>% pull,
                upr = upr/new_data %>%  filter(car_ntile == 'noncarrier_Intermediate') %>%
                   distinct(upr ) %>% pull,
                lwr = lwr/new_data %>%  filter(car_ntile == 'noncarrier_Intermediate') %>%
                   distinct(lwr ) %>% pull,
                car_ntile=car_ntile) %>%
      distinct() -> prova_final
   
}



calc_per_gene_inter_lux("severe") %>% mutate(gene = "GBA",severity="Severe")   %>%
   separate(car_ntile,c("carrier","prs"),"_") %>%
  # mutate(prs = as.numeric(prs))%>%
   mutate(gene=if_else(carrier=="noncarrier","noncarrier",gene)) %>%
   filter(gene=="GBA")%>% 
   mutate(study="LuxPark")  -> gba_severe_lux

calc_per_gene_inter_lux("mild") %>% mutate(gene = "GBA",severity="Mild")   %>%
   separate(car_ntile,c("carrier","prs"),"_") %>%
   mutate(prs = as.numeric(prs))%>%
   mutate(gene=if_else(carrier=="noncarier","noncarrier",gene))  %>%
   filter(gene=="GBA")%>%
   mutate(study="LuxPark") -> gba_mild_lux

calc_per_gene_inter_lux("low-risk") %>% mutate(gene = "GBA",severity="Low")  %>%  
   separate(car_ntile,c("carrier","prs"),"_") %>%
  mutate(prs = as.numeric(prs))%>%
   mutate(gene=if_else(carrier=="noncarier","noncarrier",gene)) %>% 
   mutate(study="LuxPark") -> gba_low_lux

png("/home/ubuntu/Mount/projects/ukbiobank_prs_rare/manuscript_pd/GBA/figure2_fin2.png", units="in", width= 9, height=6, res=300)




gba_severe %>%
   rbind(gba_mild) %>%
   rbind(gba_low) %>%
   rbind(gba_severe_lux) %>%
   rbind(gba_mild_lux) %>%
   rbind(gba_low_lux) %>%
   mutate(severity=if_else(severity=="Low","Mild",severity)) %>% 
   mutate(severity=if_else(carrier=="noncarrier","noncarrier",severity)) %>%
   mutate(severity=fct_reorder(severity,desc(pred_or))) %>% 
   mutate(study=factor(study,levels = c("UK Biobank","LuxPark"))) %>% 
   mutate(prs=factor(prs,levels = c("Low","Intermediate","High"))) %>% 
   
   ggplot(aes(pred_or,prs,group = interaction(carrier, severity), color = carrier)) +
   
   geom_point(aes(color = carrier,shape=severity),size = 2, position=position_dodge(width = 0.2))+
   geom_errorbarh(aes(xmin = lwr, xmax = upr), height = 0,alpha=0.5, position=position_dodge(width = 0.2)) +
   
   #geom_line(size = 0.8)+
   geom_vline(xintercept = 1, linetype='dotted', color = 'grey50', size=1) +
   guides(colour = guide_legend(order = 1),
          shape = guide_legend(order = 2)) +
   facet_wrap(~study, scales = "free_x") +
   coord_cartesian(xlim=c(0,5)) +

   
   # scale_x_continuous(breaks = seq(0, 5, by = 1))+
  # scale_y_continuous(trans='log2',
#                      breaks = c(0.5,1,2,4)) +
   theme_bw()+
   theme(legend.text=element_text(size=15),
         legend.title = element_text(size=15),
         axis.line = element_line(size=0.9),
         axis.title   = element_text(size=15),
         axis.text = element_text(size=15,colour = "black"),
         axis.ticks = element_line(size=1),
         axis.text.y = element_text(hjust=0,face = 'italic'),
         axis.ticks.length=unit(0.25, 'cm'),
         axis.title.x = element_text(margin = margin(t = 10, r = 0, b =10, l = 0)),
         legend.key.size = unit(1.5, 'lines'))+
         labs(x = "Odds ratio for PD" , y = "PRS categories") -> prs_car_type

set_palette(prs_car_type,c("#ED0000FF","#00468BFF","#FDAF91FF"))


dev.off()
