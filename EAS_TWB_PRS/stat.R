# Organize uk_biobank phenotype data ----

# 1.0 Load libraries ----
print("install and loading dependencies packages")

#install.packages("pacman") 
library("pacman")


pacman::p_load(tidyverse     # tidy daya
               ,broom
               , tictoc      # checking time 
               , furrr       # functional programing with map()
               , future      # parallel computing
               , arrow       # apache arrow provides powerful package for sharing data
               , data.table  # exporting data as .txt
               , ggpubr      # add themes and visualization prop. to ggplot
               , doParallel
               , here
               ,glmnet
               ,epiDisplay)    

# 2.0 Import data ----

fread("fam_gen.pop") -> fam_pop
fread("middle_east_ukb.tsv") -> mea 
fam_pop %>% separate(gen_pop,c("super","sub"),sep="_") %>% 
   left_join(mea,by=c("V1"="eid")) %>% 
   mutate(ancestry_group=case_when(sub=="JPT"~"Japanese",TRUE~ancestry_group))-> fam_pop2
pheno <- fread("LDL.HDL.TC.pheno.cov.tsv")
multi_pheno <-  fread("multi_pheno_prs.tsv")
multi_pheno %>% dplyr::select(eid,V2, pheno_cad ,pheno_t2d) -> to_join


# statin calc ----
print("source change_me.r file")
source(here::here("phenotype","organize_ukbb","change_me.R"))

#source(here::here("phenotype","organize_ukbb","test.R"))

feather_file <- file.path(export_path,"ukb_df.feather")


print("time taken to import all datasets")

tic()

# 3.0 Process data ----

ukbb_pc_df <- arrow::read_feather(feather_file,
                                  col_select = c("eid",
                                                 matches("f22019"), matches("f22027"),
                                                 matches("f22001"),matches("sex_f31"))) %>%
   filter(outliers_for_heterozygosity_or_missing_rate_f22027_0_0 != "Yes" | 
             is.na(outliers_for_heterozygosity_or_missing_rate_f22027_0_0)) %>% 
   filter(sex_chromosome_aneuploidy_f22019_0_0 != "Yes" | 
             is.na(sex_chromosome_aneuploidy_f22019_0_0)) %>%
   mutate( sex_check= case_when(
      sex_f31_0_0 == genetic_sex_f22001_0_0 ~ TRUE,
      TRUE ~ FALSE
   )) %>%
   filter(sex_check == TRUE) %>% 
   left_join(fam_pop2 %>% dplyr::select(V1,V2),by=c("eid"="V1")) 



# import data ----
taiwan_re <- fread("r2_score.csv") %>% 
   rename("full_R2"="FULL_R2","null_R2"="NULL_R2","incR2"="INCREMENTAL_R2","source"="MODEL") %>% 
   mutate(sub="TWB")
   



ldl_path="PRS/LDL/"  # path to the data
files_ldl <- dir(ldl_path, pattern = "*.best") # get file names


load_data = function(files_re,path){
files_re %>%
   map(function(x) 
      fread(file.path(path, x)) %>%
      mutate(file =x,
                 file = str_remove(file,"_META_pst_eff_a1_b0.5.genome.wide.txt.prsice.best")) %>%
      separate(file,c("trait_name","source_gwas"),sep="_", extra = "merge")) %>% 
   reduce(rbind) %>% 
   spread(source_gwas,PRS) %>%
   left_join(fam_pop2 %>% dplyr::select(V2,super,sub,ancestry_group), by=c("FID"="V2")) %>%
   inner_join(pheno_statin, by=c("FID"="IID")) -> cleaned

}

load_data(files_ldl,ldl_path) -> ldl_cleaned

ldl_cleaned %>% filter(!is.na(LDL)) %>%
   inner_join(ukbb_pc_df %>% dplyr::select(eid,V2),by=c("FID"="V2")) %>%
   left_join(to_join,by=c("FID"="V2")) %>% 
   mutate(pheno_hc=if_else(LDL >= 4.1 ,1,0))-> ldl_cleaned_no_na

# 4.0 Statsistical analysis ----
## 4.1 function to calculate full model vs null model statistics

getstatistics <- function(data,quant_pheno,prs) {
   
   fullmodel = formula(paste0(deparse(substitute(quant_pheno))," ~ ", deparse(substitute(prs)) ,
                              "+ sex + age+I(age^2) +", 
                              paste0("PC",1:4,collapse = "+")))
   nullmodel = formula(paste0(deparse(substitute(quant_pheno))," ~ ", 
                              "+ sex + age+I(age^2) +",
                              paste0("PC",1:4,collapse = "+")))
   
   
   data %>%

      nest(data = -ancestry_group) %>%
      mutate(model = map(data, ~lm(fullmodel
                                   , data = .)),
             tidied = map(model, glance)) %>%
      unnest(tidied) %>%
      dplyr::select(1,4,6) %>%
      rename("full_R2" ="r.squared")-> full
   
   
   data %>%
    #filter(super %in% c("EAS")) %>%
      nest(data = -ancestry_group) %>%
      mutate(model = map(data, ~lm(nullmodel
                                   , data = .)),
             tidied = map(model, glance)) %>%
      unnest(tidied) %>%
      dplyr::select(1,4) %>%
      rename("null_R2" ="r.squared")-> null
   
   full %>%
      full_join(null) %>%
      mutate(incR2 = full_R2 - null_R2) 
   
}

getstatistics_sub <- function(data,quant_pheno,prs) {
   
   fullmodel = formula(paste0(deparse(substitute(quant_pheno))," ~ ", deparse(substitute(prs)) ,
                              "+ sex + age+I(age^2) +", 
                              paste0("PC",1:4,collapse = "+")))
   nullmodel = formula(paste0(deparse(substitute(quant_pheno))," ~ ", 
                              "+ sex + age+I(age^2) +",
                              paste0("PC",1:4,collapse = "+")))
   
   
   data %>%
      filter(super %in% c("EAS")) %>%
      nest(data = -sub) %>%
      mutate(model = map(data, ~lm(fullmodel
                                   , data = .)),
             tidied = map(model, glance)) %>%
      unnest(tidied) %>%
      dplyr::select(1,4,6) %>%
      rename("full_R2" ="r.squared")-> full
   
   
   data %>%
      filter(super %in% c("EAS")) %>%
      nest(data = -sub) %>%
      mutate(model = map(data, ~lm(nullmodel
                                   , data = .)),
             tidied = map(model, glance)) %>%
      unnest(tidied) %>%
      dplyr::select(1,4) %>%
      rename("null_R2" ="r.squared")-> null
   
   full %>%
      full_join(null) %>%
      mutate(incR2 = full_R2 - null_R2) 
   
}

## 4.2 running function to calculate full model vs null model statistics


### LDL

getstatistics(ldl_cleaned_no_na,LDL,AFR_EAS_EUR_SAS) %>% mutate(source="AFR_EAS_EUR_SAS") -> ldl_AFR_EAS_EUR_SAS
getstatistics(ldl_cleaned_no_na,LDL,EAS_EUR) %>% mutate(source="EAS_EUR") -> ldl_EAS_EUR
getstatistics(ldl_cleaned_no_na,LDL,EAS) %>% mutate(source="EAS") -> ldl_EAS
getstatistics(ldl_cleaned_no_na,LDL,EUR) %>% mutate(source="EUR") -> ldl_EUR
getstatistics(ldl_cleaned_no_na,LDL,AFR) %>% mutate(source="AFR") -> ldl_AFR
getstatistics(ldl_cleaned_no_na,LDL,SAS) %>% mutate(source="SAS") -> ldl_SAS
# getstatistics(ldl_cleaned_no_na,LDL,final_score) %>% mutate(source="lasso") -> ldl_stepAIC

getstatistics_sub(ldl_cleaned_no_na,LDL,AFR_EAS_EUR_SAS) %>% mutate(source="AFR_EAS_EUR_SAS") -> ldl_AFR_EAS_EUR_SAS_sub
getstatistics_sub(ldl_cleaned_no_na,LDL,EAS_EUR) %>% mutate(source="EAS_EUR") -> ldl_EAS_EUR_sub
getstatistics_sub(ldl_cleaned_no_na,LDL,EAS) %>% mutate(source="EAS") -> ldl_EAS_sub
getstatistics_sub(ldl_cleaned_no_na,LDL,EUR) %>% mutate(source="EUR") -> ldl_EUR_sub
getstatistics_sub(ldl_cleaned_no_na,LDL,AFR) %>% mutate(source="AFR") -> ldl_AFR_sub
getstatistics_sub(ldl_cleaned_no_na,LDL,SAS) %>% mutate(source="SAS") -> ldl_SAS_sub
# getstatistics(ldl_cleaned_no_na,LDL,final_score) %>% mutate(source="lasso") -> ldl_stepAIC


ldl_AFR_EAS_EUR_SAS %>% 
   rbind(ldl_EAS_EUR) %>%  rbind(ldl_AFR) %>% 
   rbind(ldl_SAS) %>% rbind(ldl_EAS) %>% 
   rbind(ldl_EUR)  %>%
   mutate(source=factor(source,c("EUR","EAS","AFR","SAS","EAS_EUR","AFR_EAS_EUR_SAS","lasso"))) %>% 
   left_join(ldl_cleaned_no_na %>% count(ancestry_group)) %>% 
   filter(ancestry_group !="") %>%  filter(ancestry_group !="Japanese") %>%
   mutate(anc_group=paste(ancestry_group,n, sep="\n n=")) %>%
   mutate(anc_group=as.factor(anc_group)) %>%
   mutate(anc_group=factor(anc_group,levels = c("United Kingdom\n n=423596","Poland\n n=4095","Italy\n n=6451","Ashkenazi\n n=2359","Iran\n n=1145","India\n n=6303","Nigeria\n n=3802","Caribbean\n n=2492","China\n n=1480"))) %>%
   mutate(`incR2 (%)`=incR2*100) %>%
   ggplot(aes(anc_group,`incR2 (%)`,  fill= source)) + 
   geom_bar(position="dodge", stat="identity",width =0.7) +
   geom_errorbar( aes(x=anc_group,ymin=`incR2 (%)`-(`std.err`), ymax=`incR2 (%)`+(`std.err`),color=source), width=.2,
                  position=position_dodge(0.7))+
   theme_pubr() +
   labs(x = "",y="Incremental R2 (%)")-> ldl_r2_plot_ukb

png("/home/ubuntu/Mount/projects/ukbiobank_prs_rare/twb/figure2.png", units="in", width= 11, height=5.5, res=300)

set_palette(ldl_r2_plot_ukb,"lancet")
dev.off()

ldl_EAS_sub %>% 
   rbind(ldl_AFR_sub) %>% rbind(ldl_SAS_sub) %>% 
   rbind(ldl_EAS_EUR_sub) %>% rbind(ldl_EUR_sub) %>% 
   rbind(ldl_AFR_EAS_EUR_SAS_sub,taiwan_re %>% filter(PHENOTYPE == "LDL") %>% dplyr::select(-PHENOTYPE) %>% mutate(sigma=0.65) ) %>% 
   mutate(source=factor(source,c("EUR","EAS","AFR","SAS","EAS_EUR","AFR_EAS_EUR_SAS"))) %>%
   left_join(ldl_cleaned_no_na %>% count(sub)) %>% 
   left_join(ldl_cleaned_no_na %>% count(super)) %>% 
   mutate(n=if_else(sub=="TWB",68978,as.double(n))) %>%
   filter(n>400) %>%
   mutate(sub=factor(sub,levels = c("TWB","CHS","KHV"))) %>%
   
  # mutate(sub=factor(sub,levels = c("TWB","CHS","CDX","CHB","JPT","KHV"))) %>%
   mutate(anc_group=paste(sub,n, sep="\n n=")) %>%
   mutate(anc_group=fct_reorder(anc_group,desc(n))) %>%
   mutate(`incR2 (%)`=incR2*100) %>%
   filter(sub != "") %>%
   ggplot(aes(anc_group,`incR2 (%)`,  fill= source)) + 
   geom_bar(position="dodge", stat="identity",width =0.7) +
   geom_errorbar( aes(x=anc_group,ymin=`incR2 (%)`-(`std.err`), ymax=`incR2 (%)`+(`std.err`),color=source), width=.2,
                  position=position_dodge(0.7))+
   theme_pubr() +
   labs(x = "",y="Incremental R2 (%)")-> ldl_r2_plot_sub


set_palette(ldl_r2_plot_sub,"lancet")


## 4.3 performing Corelation and analysis an dploting using corrplot




ldl_cleaned_no_na %>% filter(ancestry_group=="China")  %>% dplyr::select(LDL,LDL_adj,AFR,SAS,EUR,EAS,EAS_EUR,AFR_EAS_EUR_SAS) %>% na.omit -> ldl_cor
corrplot(cor(ldl_cor), method = 'square',  tl.col = 'black',type = 'lower',addCoef.col = 'black', cl.pos = 'n',cl.ratio = 0.2, tl.srt = 45)



## 4.4 calculating ORS for full and null models


# function to make 3 quantiles and get OR from model
getstatistics_or <- function(data,quant_pheno,prs) {
   
   fullmodel = formula(paste0(deparse(substitute(quant_pheno))," ~ ", "prs_ntile" ,
                              "+ sex + age+I(age^2) +", 
                              paste0("PC",1:4,collapse = "+")))

   fullmodel_prs = formula(paste0(deparse(substitute(quant_pheno))," ~ ", "prs" ,
                              "+ sex + age+I(age^2) +", "statin +" ,
                              paste0("PC",1:4,collapse = "+")))
   data %>%
      mutate(prs = scale(  {{ prs }} )) %>% 
      mutate(prs_ntile = as.factor(ntile(  {{ prs }}  ,10))) %>%
      mutate(ldl_ntile = case_when(
         LDL < 3.4 ~ "<3.4",
         LDL >= 3.4 & LDL < 4.1 ~ "3.4-<4.1",
         LDL > 4.1 ~ ">4.1")) %>%
      mutate(prs_ntile = factor(prs_ntile),
             prs_ntile= fct_recode(prs_ntile,
                                   "low"      = "1",
                                   "low"   = "2" ,
                                   "medium"   = "3" ,
                                   "medium"   = "4" ,
                                   "medium"   = "5" ,
                                   "medium"   = "6",
                                   "medium"   = "7" ,
                                   "medium"   = "8" ,
                                   "high"   = "9" ,
                                   "high"     = "10")) %>%
       mutate( prs_ldl_ntile = factor(paste(prs_ntile,ldl_ntile,sep = "_")))  %>%
      # mutate(ldl_ntile = factor(ldl_ntile)) %>%
      mutate(prs_ntile = relevel(prs_ntile, ref = "medium")) %>%
      mutate(prs_ldl_ntile = relevel(prs_ldl_ntile, ref = "medium_<3.4")) %>%
      
      filter(ancestry_group %in% c("United Kingdom","China"))  %>%
   
      #group_by(prs_ntile) %>% 
      nest(data = -ancestry_group) %>%
      #nest(data = -c(ancestry_group,prs_ntile)) %>%
      mutate(model = map(data, ~glm(fullmodel_prs
                                   , data = .,family = "binomial")),
             tidied = map(model, tidy,exponentiate=T)) %>%
      unnest(tidied) -> full
   
   
}



getstatistics_or(ldl_cleaned_no_na,pheno_hc,AFR_EAS_EUR_SAS) %>% mutate(source="AFR_EAS_EUR_SAS") -> or_ldl_AFR_EAS_EUR_SAS
getstatistics_or(ldl_cleaned_no_na,pheno_hc,EAS) %>% mutate(source="EAS") -> or_ldl_EAS
getstatistics_or(ldl_cleaned_no_na,pheno_hc,EUR) %>% mutate(source="EUR") -> or_ldl_EUR



or_ldl_AFR_EAS_EUR_SAS %>%
   rbind(or_ldl_EAS) %>% 
   rbind(or_ldl_EUR) %>%
   # rbind(or_ldl_AFR) %>% 
   # rbind(or_ldl_SAS) %>%
   dplyr::select(ancestry_group,term,estimate,`std.error`,source,`p.value`) %>% 
   filter(str_detect(term,"prs")) %>% 
   #mutate(term=str_remove(term,"prs_ntile")) %>% 
   filter(ancestry_group %in% c("China","United Kingdom")) %>% 
   rbind(twb_eas) %>%
   rbind(twb_eur) %>%
   rbind(twb_multi) %>%
   mutate(ancestry_group=as.factor(ancestry_group)) %>%
   mutate(ancestry_group=factor(ancestry_group,levels = c("United Kingdom","China","TWB"))) %>%
   
   ggplot(aes(ancestry_group,estimate,  color=source)) + 
   geom_point( alpha=5,position=position_dodge(-0.4),size =5) +
   #facet_grid(ancestry_group ~ source) +
   
   geom_errorbar( aes(x=ancestry_group,ymin=estimate-(`std.error`/2), ymax=estimate+(`std.error`/2),color=source), width=.2,
                  position=position_dodge(-0.4))+
   #geom_hline(yintercept = 1, linetype="dotted", color = "grey50", size=1) +
   #scale_y_continuous(breaks = seq(from =0, to = 10, by = 2)) +
   theme_bw() +
   
   labs(x = "",y="Odds ratio for HC") -> or_hc

png("/home/ubuntu/Mount/projects/ukbiobank_prs_rare/twb/Figures/figure3.png", units="in", width= 8, height=5, res=300)

set_palette(or_hc,c("#FDAF91FF","#ED0000FF","#00468BFF"))
dev.off()

getstatistics_or(ldl_cleaned_no_na,pheno_cad,AFR_EAS_EUR_SAS) %>% mutate(source="AFR_EAS_EUR_SAS") -> or_ldl_AFR_EAS_EUR_SAS
getstatistics_or(ldl_cleaned_no_na,pheno_cad,EAS) %>% mutate(source="EAS") -> or_ldl_EAS
getstatistics_or(ldl_cleaned_no_na,pheno_cad,EUR) %>% mutate(source="EUR") -> or_ldl_EUR


or_ldl_AFR_EAS_EUR_SAS %>%
   rbind(or_ldl_EAS) %>% 
   rbind(or_ldl_EUR) %>%
   dplyr::select(ancestry_group,term,estimate,`std.error`,source,`p.value`) %>% 
   filter(str_detect(term,"prs")) %>% 
   mutate(term=str_remove(term,"prs_ntile")) %>% 
   filter(ancestry_group %in% c("China","United Kingdom")) %>% 
   rbind(twb_eas) %>%
   rbind(twb_eur) %>%
   rbind(twb_multi) %>%
   ggplot(aes(ancestry_group,estimate,  color=source)) + 
   geom_point( alpha=5,position=position_dodge(-0.4),size =5) +
   #facet_grid(ancestry_group ~ source) +
   
   geom_errorbar( aes(x=ancestry_group,ymin=estimate-(`std.error`), ymax=estimate+(`std.error`),color=source), width=.2,
                  position=position_dodge(-0.4))+
   #geom_hline(yintercept = 1, linetype="dotted", color = "grey50", size=1) +
   #scale_y_continuous(breaks = seq(from =0, to = 10, by = 2)) +
   theme_pubr(legend = 'right') +
   labs(x = "")






## 4.5 function to  calculate ORS for full and null models


getstatistics_or <- function(data,quant_pheno,prs) {
   
   fullmodel = formula(paste0(deparse(substitute(quant_pheno))," ~ ", "ldl_ntile" ,
                              "+ sex + age+I(age^2) +", 
                              paste0("PC",1:4,collapse = "+")))
   
   fullmodel_prs = formula(paste0(deparse(substitute(quant_pheno))," ~ ", "prs" ,
                                  "+ sex + age+I(age^2) +", "statin +" ,
                                  paste0("PC",1:4,collapse = "+")))
   data %>%
      mutate(prs = scale(  {{ prs }} )) %>% 
      mutate(prs_ntile = as.factor(ntile(  {{ prs }}  ,10))) %>%
      mutate(ldl_md=LDL_adj*38.67 ) %>%
      mutate(ldl_ntile = case_when(
         LDL < 3.4 ~ "<3.4",
         LDL >= 3.4 & LDL < 4.1 ~ "3.4-<4.1",
         LDL >= 4.1 ~ ">4.1")) %>%
      mutate(prs_ntile = factor(prs_ntile),
             prs_ntile= fct_recode(prs_ntile,
                                   "low"      = "1",
                                   "medium"   = "2" ,
                                   "medium"   = "3" ,
                                   "medium"   = "4" ,
                                   "medium"   = "5" ,
                                   "medium"   = "6",
                                   "medium"   = "7" ,
                                   "medium"   = "8" ,
                                   "medium"   = "9" ,
                                   "high"     = "10")) %>%
      mutate( prs_ldl_ntile = factor(paste(prs_ntile,ldl_ntile,sep = "_")))  %>%
      mutate(ldl_ntile = factor(ldl_ntile)) %>%
      mutate(ldl_ntile = relevel(ldl_ntile, ref = "<3.4")) %>%
      mutate(prs_ntile = relevel(prs_ntile, ref = "low")) %>%
         
      #mutate(prs_ldl_ntile = relevel(prs_ldl_ntile, ref = "low_<3.4")) %>%
      
      filter(ancestry_group %in% c("United Kingdom","India"))  %>%
      
      #group_by(prs_ntile) %>% 
      #nest(data = -ancestry_group) %>%
      nest(data = -c(ancestry_group,prs_ntile)) %>%
      mutate(model = map(data, ~glm(fullmodel
                                    , data = .)),
             tidied = map(model, tidy,exponentiate=T)) %>%
      unnest(tidied) -> full
   
   
}

ldl_cleaned_no_na %>%
   left_join(multi_pheno %>% dplyr::select(V2,prs_cad ,prs_t2d),by=c("FID"="V2"))  -> ldl_cleaned_no_na1

getstatistics_or(ldl_cleaned_no_na1,pheno_cad,prs_cad) %>% mutate(source="EUR") -> or_ldl_EUR

or_ldl_EUR %>%
   dplyr::select(ancestry_group,prs_ntile,term,estimate,`std.error`,source,`p.value`) %>% 
   filter(str_detect(term,"ldl_ntile")) %>% 
   mutate(term=str_remove(term,"ldl_ntile")) %>% 
   filter(ancestry_group %in% c("United Kingdom")) %>% 
   ggplot(aes(ancestry_group,estimate,  color=source)) + 
   geom_point( alpha=5,position=position_dodge(-0.4),size =5) +
   #facet_grid(ancestry_group ~ source) +
   
   geom_errorbar( aes(x=ancestry_group,ymin=estimate-(`std.error`), ymax=estimate+(`std.error`),color=source), width=.2,
                  position=position_dodge(-0.4))+
   #geom_hline(yintercept = 1, linetype="dotted", color = "grey50", size=1) +
   #scale_y_continuous(breaks = seq(from =0, to = 10, by = 2)) +
   theme_pubr(legend = 'right') +
   labs(x = "")


png("/home/ubuntu/Mount/projects/ukbiobank_prs_rare/twb/Figures/figure1_final.png", units="in", width= 11, height=5, res=300)

set_palette(ldl_r2_plot_ukb,"lancet")


dev.off()


png("/home/ubuntu/Mount/projects/ukbiobank_prs_rare/twb/Figures/figure2_final.png", units="in", width= 5, height=4, res=300)

set_palette(ldl_r2_plot_sub,"lancet")

dev.off()


## 4.6 calculate ORS for full and null models in TWB


twb <- fread("/home/ubuntu/Mount/Multi_ancestry_PRS/Pheno/twb.csv") %>% 
   mutate(pheno_hc_ldl=if_else(`LDL(mg/dL)` >= 160,1,0))

getstatistics_or_twb <- function(data,quant_pheno,prs) {
   
   fullmodel_prs = formula(paste0(deparse(substitute(quant_pheno))," ~ ", "prs" ,
                                  "+ SEX + AGE+I(AGE^2) +", 
                                  paste0("PC",1:4,collapse = "+")))
   data %>%
      mutate(prs = scale(  {{ prs }} )) %>% 
      mutate(model = glm(fullmodel, data = .),
             tidied = tidy(model, exponentiate=T)) %>%
      unnest(tidied) -> full
   
}


getstatistics_or_twb <- function(data,quant_pheno,prs) {
   
fullmodel_prs = formula(paste0(deparse(substitute(quant_pheno))," ~ ", "prs" ,
                               "+ SEX + AGE+I(AGE^2) +", 
                                  paste0("PC",1:4,collapse = "+")))
   twb %>%
      mutate(prs = scale(  {{ `LDL/EAS` }} )) %>% 
      mutate(model = glm(fullmodel_prs, data = .,family = "binomial"),
             tidied = tidy(model, exponentiate=T)) %>%
      unnest(tidied) -> full
}


model_eas <- glm(pheno_hc_ldl~scale(`LDL/EAS`)+ SEX + AGE+I(AGE^2) + PC1+PC2+PC3+PC4,data=twb,family = "binomial")
model_eur <- glm(pheno_hc_ldl~scale(`LDL/EUR`)+ SEX + AGE+I(AGE^2) + PC1+PC2+PC3+PC4,data=twb,family = "binomial")
model_multi <- glm(pheno_hc_ldl~scale(`LDL/AFR_EAS_EUR_SAS`)+ SEX + AGE+I(AGE^2) + PC1+PC2+PC3+PC4,data=twb,family = "binomial")

tidy(model_eas,exponentiate = T) %>%    dplyr::select(term,estimate,`std.error`,`p.value`) %>% 
   filter(str_detect(term,"LDL/EAS")) %>% 
   mutate(term="prs",source="EAS",ancestry_group="TWB") -> twb_eas
   
tidy(model_eur,exponentiate = T) %>%   dplyr::select(term,estimate,`std.error`,`p.value`) %>% 
   filter(str_detect(term,"LDL/EUR")) %>% 
   mutate(term="prs",source="EUR",ancestry_group="TWB") ->twb_eur


tidy(model_multi,exponentiate = T) %>%   dplyr::select(term,estimate,`std.error`,`p.value`) %>% 
   filter(str_detect(term,"TC/AFR_EAS_EUR_SAS")) %>% 
   mutate(term="prs",source="AFR_EAS_EUR_SAS",ancestry_group="TWB") -> twb_multi

## 4.6 calculate means of LDL per PRR deciles in TWB

twb %>%
   mutate(ldl=`LDL(mg/dL)`) %>% 
      mutate(ldl_ntile = case_when(
         ldl < 100 ~ "<100",
         #ldl >= 100 &  ldl < 130~ "100-<130",
         #ldl >= 130 &  ldl < 160~ "130-<160",
         ldl >= 100 &  ldl < 160~ "160-<190",
         ldl >= 190 ~ ">190",
         ),
   prs_ntile= ntile(scale(`LDL/EUR`),5)) -> twb_hc


make_model <- function(data){
 lm(ldl~prs_ntile + SEX + AGE+I(AGE^2) + PC1+PC2+PC3+PC4,data = data)
}

twb_hc %>% 
   group_by(ldl_ntile) %>% 
   nest() %>% 
   mutate(lm = map(data,
                   make_model)) %>% 
   mutate(tidy = map(lm,
                     broom::tidy,exponentiate = T)) %>% 
   unnest(tidy) %>% 
   filter(term=="prs_ntile")
   


ldl_cleaned_no_na %>% 
   mutate(prs_ntile=ntile(scale(EUR),10)) %>% 

   filter(ancestry_group %in% c("United Kingdom")) %>% 
   group_by(prs_ntile) %>%
   summarise(mean_ldl_eur=mean(LDL)) %>% 

   mutate(super="UKB_UK")-> uk_eur_ldl


ldl_cleaned_no_na %>% 
   mutate(prs_ntile=ntile(scale(EAS),10)) %>% 
   filter(ancestry_group %in% c("United Kingdom")) %>% 
   group_by(prs_ntile) %>%
   summarise(mean_ldl_eas=mean(LDL)) %>% 

   mutate(super="UKB_UK")-> uk_eas_ldl



LDL/AFR_EAS_EUR_SAS

twb_hc %>% 
   mutate(prs_ntile=ntile(scale(`LDL/EUR`),10)) %>% 
   mutate(ldl_mmol=`LDL(mg/dL)`/38) %>%
   group_by(prs_ntile) %>%
   summarise(mean_ldl_eur=mean(ldl_mmol)) %>% 
   mutate(super="EAS_PRS vs EUR_PRS")-> twb_eur_ldl


twb_hc %>% 
   mutate(prs_ntile=ntile(scale(`LDL/EAS`),10)) %>% 
   mutate(ldl_mmol=`LDL(mg/dL)`/38) %>%
   group_by(prs_ntile) %>%
   summarise(mean_ldl_eas=mean(ldl_mmol))%>% 
   mutate(mean_ldl_AFR_EAS_EUR_SAS=NA) %>%
   mutate(super="EAS_PRS vs EUR_PRS")  -> twb_eas_ldl

twb_hc %>% 
   mutate(prs_ntile=ntile(scale(`LDL/AFR_EAS_EUR_SAS`),10)) %>% 
   mutate(ldl_mmol=`LDL(mg/dL)`/38) %>%
   group_by(prs_ntile) %>%
   summarise(mean_ldl_AFR_EAS_EUR_SAS=mean(ldl_mmol)) %>% 
   mutate(super="EAS_PRS vs Multi_PRS")-> twb_eur_ldl2


twb_hc %>% 
   mutate(prs_ntile=ntile(scale(`LDL/EAS`),10)) %>% 
   mutate(ldl_mmol=`LDL(mg/dL)`/38) %>%
   group_by(prs_ntile) %>%
   summarise(mean_ldl_eas=mean(ldl_mmol))%>% 
   mutate(mean_ldl_eur=NA) %>%
   mutate(super="EAS_PRS vs Multi_PRS")  -> twb_eas_ldl2



png("/home/ubuntu/Mount/projects/ukbiobank_prs_rare/twb/Figures/figure3_final.png", units="in", width= 6.5, height=3.5, res=300)

twb_eur_ldl %>% 
   left_join(twb_eas_ldl) %>% dplyr::select(-mean_ldl_AFR_EAS_EUR_SAS) %>%
   left_join(twb_eur_ldl2 %>% dplyr::select(-super)) %>% 
   # rbind(twb_eas_ldl2 %>%
   # left_join(twb_eur_ldl2)) %>%
   # 
   mutate(prs_ntile=as.factor(prs_ntile)) %>% 
   pivot_longer(cols=c(mean_ldl_eur,mean_ldl_eas,mean_ldl_AFR_EAS_EUR_SAS),names_to="source",values_to = "values") %>% 
   mutate(source=case_when(
      source == "mean_ldl_eur" ~ "EUR",
      source == "mean_ldl_eas" ~ "EAS",
      source == "mean_ldl_AFR_EAS_EUR_SAS" ~ "Multi"
      )) %>% 
   group_by(prs_ntile) %>% 
   mutate(min_mean=min(values),max_mean=max((values)))%>%
   ungroup()%>%
   ggplot() +
   geom_segment( aes(x=prs_ntile, xend=prs_ntile, y=min_mean, yend=max_mean), color="grey") +
   geom_point( aes(x=prs_ntile, y=values, color=source), size=3 ) +
   
   coord_flip() +
   # scale_color_manual(values = c('orange', 'purple')) +
   theme_bw()+
   
  # facet_wrap(~super,ncol = 1,scales = "free") +
   
   xlab("") +
   ylab("LDL mean (mmol/L)") + 
   labs(color='PRS source')  -> means_plot
   # you should specify the color also
   # scale_color_manual(labels = c("EUR","EAS", "Multi")
   #                    ,values = c("#ED0000FF", "#00468BFF","#E7B800"))

set_palette(means_plot,c("#ED0000FF", "#00468BFF","#FDAF91FF"))

dev.off()


png("/home/ubuntu/Mount/projects/ukbiobank_prs_rare/twb/Figures/figure3_final.png", units="in", width= 6, height=5, res=300)


ggarrange(set_palette(or_hc,c("#FDAF91FF","#ED0000FF","#00468BFF")), set_palette(plot_car_hist,c("#FDAF91FF","#ED0000FF","#00468BFF")), 
          labels = c("A", "B"),
          ncol = 1, nrow = 2)


dev.off()


