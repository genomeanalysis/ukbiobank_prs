# Manuscript: "Trans-ancestry polygenic models for the prediction of LDL blood levels: An analysis of the UK Biobank and Taiwan Biobank"

The script for the analysis is available in this repo.

## Getting started

To run the script:

1) [UK biobank phenotypic data](http://www.ukbiobank.ac.uk/about-biobank-uk/) is needed . Restrictions apply to the availability of these data, which were used under license for the current study (Project ID: 52446).  

2) All the analysis and visualizations are donw using R 4.2.2, and script is placed in this repo (stat.R); with the following Packages used to run the script:

tidyverse, furrr, future, data.table, broom, pROC, forestmodel, epiDisplay, survival, survminer, knitr, kableExtra, and caret

## Output

The main figures of this manuscript, can be found in directory `Figures`.

## Authors of this paper

Emadeldin Hassanin, Ko-Han Lee, Tzung-Chien Hsieh, Rana Aldisi, Yi-Lun Lee, Dheeraj Bobbili, Peter Krawitz, Patrick May, Chien-Yu Chen, Carlo Maj


## License
This project is available under the [MIT License](https://gitlab.lcsb.uni.lu/genomeanalysis/ukbiobank_prs/LICENSE).




