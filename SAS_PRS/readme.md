# Manuscript: "Transferability of European-derived cardiometabolic polygenic risk scores in the South Asians and their interplay with family history"

The script for the analysis is available in this repo.

## Getting started

To run the script:

1) [UK biobank phenotypic data](http://www.ukbiobank.ac.uk/about-biobank-uk/) is needed . Restrictions apply to the availability of these data, which were used under license for the current study (Project ID: 52446).  

2) All the analysis and visualizations are donw using R 4.2.2, and script is placed in this repo (sas_ukb.R); with the following Packages used to run the script:

tidyverse, furrr, future, data.table, broom, pROC, forestmodel, epiDisplay, survival, survminer, knitr, kableExtra, and caret

## Output

The main figures of this manuscript, can be found [here](https://gitlab.lcsb.uni.lu/genomeanalysis/ukbiobank_prs/SAS_PRS/Figures).

## Authors of this paper

Emadeldin Hassanin, Carlo Maj, Peter Krawitz, Patrick May, Dheeraj Reddy Bobbili


## License
This project is available under the [MIT License](https://gitlab.lcsb.uni.lu/genomeanalysis/ukbiobank_prs/LICENSE).




